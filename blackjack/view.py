def viewHand(player, isDealer):
    if isDealer:
        card = player.hand[0]
        print(f"The Dealer's hand is {card.desc} and ?")
        print("")
        return
    print(f"This is {player.name}'s hand")
    tot = sum([card.value for card in player.hand])
    for card in player.hand:
        print(f'{card.desc}')
    print(f'Total: {tot}')
    print("")

def showBank(player):
    print(f"{player.name} now has ${player.bank} ")
