def whoWon(player, dealer):
    tot1 = sum([card.value for card in player.hand])
    tot2 = sum([card.value for card in dealer.hand])
    if tot1 == tot2:
        print("Tie")
        return False
    if tot2 > tot1 and tot2 <= 21:
        print("Dealer Wins")
        return False
    else:
        print(f'{player.name} Wins!')
        return True

def isWin(player):
    total = sum([card.value for card in player.hand])
    if total > 21:
        for card in player.hand:
            if card.value == 11:
                card.value = 1
                return isWin(player)
        print(f'{total} | BUST!')
        return True
    if total == 21:
        print(f'{total} | BLACKJACK')
        return True
    print(f'{total} | HIT?')
    return False

def isBlackJack(player):
    total = sum([card.value for card in player.hand])
    if total == 21:
        return True
