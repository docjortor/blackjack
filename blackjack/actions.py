import random
from models import Card, Player
from config import DEALER_STARTING_AMOUNT, PLAYER_STARTING_AMOUNT
from view import viewHand, showBank
from integrity import whoWon, isWin, isBlackJack


def createPlayer():
    players = []
    name = input("Enter player name: ")
    print("")
    players.append(Player(name, [],PLAYER_STARTING_AMOUNT))
    players.append(Player('Dealer', [],DEALER_STARTING_AMOUNT))
    return players
    
def buildDeck():
    deck = []
    a = {'2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, '10':10, 'Jack':10, 'Queen':10, 'King':10, 'Ace':11}
    for name in a:
        deck.append(Card(a[name],'Hearts', name))
        deck.append(Card(a[name],'Spades', name))
        deck.append(Card(a[name],'Diamonds', name))
        deck.append(Card(a[name],'Clubs', name))
    
    return deck

def shuffleDeck(deck):
    random.shuffle(deck)
    return deck

def dealDeck(deck, players):
    hands = 0
    while hands < 2:
        for p in players:
            p.hand.append(deck.pop())
        hands += 1

    return deck

def hitMe(deck, player):
    player.hand.append(deck.pop())
    print(player.hand[-1].name)
    return deck

def changeBank(player, amount):
    player.bank += amount
    print(f"{player.name} now has ${player.bank} ")

def play():
    players = createPlayer()
    player = players[0]
    dealer = players[1]
    print("Each hand costs $10")
    count = 0
    while True:
        print("")
        changeBank(player, -10)
        deck = buildDeck()
        print("Shuffling Deck!")
        shuffledDeck = shuffleDeck(deck)
        print("Dealing Deck!")
        print("")
        dealDeck(shuffledDeck,players)
        viewHand(player, False)
        viewHand(dealer, True)
        while True:
            playerWin = isWin(player)
            if playerWin:
                if isBlackJack(player):
                    changeBank(player,30)
                break
            else:
                key = input("HIT? y/n: ")
                if key.lower() == 'y':
                    hitMe(deck,player)
                else:
                    dealerTotal = sum([card.value for card in dealer.hand])
                    playerTotal = sum([card.value for card in player.hand])
                    while dealerTotal <= playerTotal:
                        hitMe(shuffledDeck,dealer)
                        dealerTotal += dealer.hand[-1].value
                    viewHand(player, False)
                    print("")
                    viewHand(dealer, False)
                    print("")
                    if whoWon(player, dealer):
                        changeBank(player, 20)
                    break
        count += 1
        if player.bank == 0:
            print("You lose")
            print(f"You lasted {count} turns. ")
            break
        playagain = input("Play again? (y/n): ")
        print("")
        if playagain.lower() == 'n':
            print(f"You lasted {count} turns. ")
            break
        for p in players:
            p.hand.clear()
