class Card:
    def __init__(self, value: int, suit: str, name: str):
        self.value = value
        self.suit = suit
        self.name = name
        self.desc = f'{name} of {suit}'

class Player:
    def __init__(self, name: str, hand: list, bank: int):
        self.name = name
        self.hand = hand
        self.bank = bank
